from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from DataCollector import DataCollector
from ui_generalinfo import Ui_generalInfo
import time

"""
Widget odpowiedzialny za wypisanie danych o locie na ekran
"""


class GeneralDataWidget(QWidget):
    def __init__(self, base=None):
        QWidget.__init__(self, base)
        self.ui = Ui_generalInfo()
        self.ui.setupUi(self)
        self.dataCollector = None

    """
    Ustawia referencje do źródła danych
    Argumenty:
        - obiekt klasy DataCollector
    """

    def setDataCollector(self, dataCollector):
        self.dataCollector = dataCollector

    """
    Slot wywołujący aktualizacje widgetu
    """

    def updateRequest(self):
        data = self.dataCollector.getLastDataRow()
        self.refreshEnginePowerAndMechanical(data)
        self.refreshLocation(data)
        self.refreshOrientation(data)
        self.refreshAltAndSpeed(data)
        self.refreshTime(data)

    """
    Metoda aktualizująca część odpowiedzialną za moc silników i mechanikę
    Argumenty:
        - wiersz z danymi
    """

    def refreshEnginePowerAndMechanical(self, data):
        if "throttle-left" in data:
            self.ui.leftEngineProgress.setValue(data["throttle-left"] * 100)
        else:
            self.ui.leftEngineProgress.setValue(0)

        if "throttle-right" in data:
            self.ui.rightEngineProgress.setValue(data["throttle-right"] * 100)
        else:
            self.ui.rightEngineProgress.setValue(0)

        if "flap-pos-norm" in data:
            self.ui.flapsProgress.setValue(data["flap-pos-norm"] * 100)
        else:
            self.ui.flapsProgress.setValue(0)

        if "gear-down" in data:
            if data["gear-down"] == 1:
                self.ui.gearState.setText("down")
            else:
                self.ui.gearState.setText("up")
        else:
            self.ui.gearState.setText("unknown")

    """
    Metoda aktualizująca część odpowiedzialną za lokalizację samolotu
    Argumenty:
        - wiersz z danymi
    """

    def refreshLocation(self, data):
        if "latitude-deg" in data:
            self.ui.latitudeDegree.setText("{0:.4f}".format(data["latitude-deg"]))
        else:
            self.ui.latitudeDegree.setText("unknown")

        if "longitude-deg" in data:
            self.ui.longitudeDegree.setText("{0:.4f}".format(data["longitude-deg"]))
        else:
            self.ui.longitudeDegree.setText("unknown")

    """
    Metoda aktualizująca część odpowiedzialną za orientację samolotu w przestrzeni
    Argumenty:
        - wiersz z danymi
    """

    def refreshOrientation(self, data):
        if "roll-deg" in data:
            self.ui.rollDegree.setText("{0:.2f}".format(data["roll-deg"]))
        else:
            self.ui.rollDegree.setText("unknown")

        if "heading-deg" in data:
            self.ui.headingDegree.setText("{0:.2f}".format(data["heading-deg"]))
        else:
            self.ui.headingDegree.setText("unknown")

        if "pitch-deg" in data:
            self.ui.pitchDegree.setText("{0:.2f}".format(data["pitch-deg"]))
        else:
            self.ui.pitchDegree.setText("unknown")

    """
    Metoda aktualizująca część odpowiedzialną za wysokość i prędkość samolotu
    Argumenty:
        - wiersz z danymi
    """

    def refreshAltAndSpeed(self, data):
        if "airspeed-kt" in data:
            self.ui.speedLCD.display((int)(data["airspeed-kt"] * 1.85200))
        else:
            self.ui.speedLCD.display(-1)

        if "altitude-to-ground-ft" in data:
            self.ui.altToGroundLCD.display((int)(data["altitude-to-ground-ft"] * 0.3048))
        else:
            self.ui.altToGroundLCD.display(-1)

        if "altitude-ft" in data:
            self.ui.altLCD.display((int)(data["altitude-ft"] * 0.3048))
        else:
            self.ui.altLCD.display(-1)

    """
    Metoda aktualizująca część odpowiedzialną za czas w którym przechwycono dane
    Argumenty:
        - wiersz z danymi
    """

    def refreshTime(self, data):
        if "time" in data:
            self.ui.timeLabel.setText((data["time"].strftime("%X %x")))
