from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtNetwork import QUdpSocket, QHostAddress
import datetime

"""
Klasa odpowiedzialna za przechwytywanie danych poprzez UDP
"""


class DataProvider(QObject):
    """
    Sygnał emitowany w przypadku pobrnia nowych danych
    Argumenty:
        - słownik indeksowany nazwami argumentów zawierający wiersz informacji o locie
    """
    newData = pyqtSignal(dict)

    def __init__(self, base=None):
        super(DataProvider, self).__init__(base)
        self.socket = QUdpSocket()
        self.socket.readyRead.connect(self._receiveAndParse)
        self.paramsNames = [
            "throttle-left",
            "throttle-right",
            "latitude-deg",
            "longitude-deg",
            "altitude-ft",
            "altitude-to-ground-ft",
            "roll-deg",
            "pitch-deg",
            "heading-deg",
            "airspeed-kt",
            "flap-pos-norm",
            "gear-down",
            "brake-parking"
        ]

    """
    Metoda odpowiedzialna za nawiązanie połączenia UDP
    Argumenty:
        - IP - adres IP hosta
        - port - port na którym nasłuchuje aplikacja
    """

    def connectTo(self, IP, port):
        adres = QHostAddress(IP)
        self.socket.bind(adres, port)

    """
    Metoda zrywająca połączenie sieciowe
    """

    def disconnect(self):
        self.socket.abort()

    """
    Slot odpowiedzialny za odczytanie odebranego datagramu i przetworzenie go na
    słownik indeksowany po kluczach będących nazwami parametrów lotu.
    Jeżeli odczyt się powiódł emitowany jest sygnał newData(dict)
    """

    def _receiveAndParse(self):
        datagram, *host = self.socket.readDatagram(8000)
        datagram = datagram.decode()
        datagram = str(datagram)

        data = dict()
        errorOccur = False
        for key, valueString in zip(self.paramsNames, datagram.split(":")):
            try:
                data[key] = float(valueString)
            except ValueError:
                errorOccur = True

        if errorOccur == False:
            data["time"] = datetime.datetime.now()
            self.newData.emit(data)
