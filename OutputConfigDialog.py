from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

# for quick IP validation

"""
Klasa odpowiedzialna za wyświetlenie okna dialogowego.
"""


class OutputConfigDialog(QDialog):
    def __init__(self, base=None):
        super(OutputConfigDialog, self).__init__(base)

        self.updateInterval = 1;
        self.filePath = "./data.txt"

        self.myLayout = QVBoxLayout(self)
        self.myLayout.addWidget(QLabel("Output file directory:"))
        self.filePathButton = QPushButton(self)
        self.filePathButton.setText("select")

        self.myLayout.addWidget(self.filePathButton)
        self.myLayout.addWidget(QLabel("Update interval in seconds:"))

        self.updateIntervalField = QSpinBox(self)
        self.updateIntervalField.setRange(1, 60)
        self.updateIntervalField.setValue(1)

        self.myLayout.addWidget(self.updateIntervalField)
        self.applyButton = QPushButton(base)
        self.applyButton.setText("apply")
        self.myLayout.addWidget(self.applyButton)

        self.applyButton.clicked.connect(self.onApplyRequested)
        self.filePathButton.clicked.connect(self.onFilePathButton_clicked)

    """
    Slot wywoływany przy wciśnięciu przycisku Apply. Zamyka dialog.
    """

    def onApplyRequested(self):
        self.updateInterval = self.updateIntervalField.value()
        self.accept()

    """
    Zwraca wczytaną konfigurację.
    Return:
        - krotka zawierająca ściężką do pliku i okres próbkowania
    """

    def getData(self):
        return self.filePath, self.updateInterval

    """
    Slot wywoływany jeśli wciśnięto przycisk wyboru ścieżki pliku. Wyświetla odpowiedni
    dialog.
    """

    def onFilePathButton_clicked(self):
        path, extra = QFileDialog.getSaveFileName()
        self.filePath = path

    """
    Metoda statyczna wywołująca okno dialogowe
    """

    @staticmethod
    def showDialog(base=None):
        dialog = OutputConfigDialog(base)
        dialog.exec_()
        return dialog
