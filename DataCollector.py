import collections
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import os.path
import pickle
import threading

"""
Klasa odpowiedzialna za zbieranie danych dotyczących połączenia
"""


class DataCollector(QObject):
    """
    Sygnał emitowany jeśli nastapiło próbkowanie nowych danych
    """
    newDataInside = pyqtSignal()

    """
    Metoda zapisuje stan wewnętrznego bufora danych do pliku co 60 próbek
    """

    def appendDataToFile(self):
        try:
            if self.outputPath is None:
                return
            if os.path.isfile(self.outputPath):

                file = open(self.outputPath, 'rb')
                dataToSave = pickle.load(file)

                for key in dataToSave:
                    dataToSave[key] = dataToSave[key] + self.valueBuffer[key]
                    self.valueBuffer[key] = list()
                file.close()

            else:
                dataToSave = dict()
                for key in self.valueBuffer:
                    dataToSave[key] = self.valueBuffer[key]
                    self.valueBuffer[key] = list()
            file = open(self.outputPath, 'wb')
            pickle.dump(dataToSave, file)
            file.close()
            print("Saving")
        except:
            print("Saving to file error")

        self.savingTimer.singleShot(self.timeIntervalInSeconds * 1000 * 10, self.appendDataToFile)

    def __init__(self, base=None):
        QObject.__init__(self, base)
        self.outputPath = None
        self.paramsNames = [
            "throttle-left",
            "throttle-right",
            "latitude-deg",
            "longitude-deg",
            "altitude-ft",
            "altitude-to-ground-ft",
            "roll-deg",
            "pitch-deg",
            "heading-deg",
            "airspeed-kt",
            "flap-pos-norm",
            "gear-down",
            "brake-parking",
            "time"]

        self.valueBuffer = dict()
        self.updateTimer = QTimer()
        self.savingTimer = QTimer()
        self.lastRow = None

        for key in self.paramsNames:
            self.valueBuffer[key] = list()

    """
    Metoda dodająca do bufora ostatni wczytany wiersz i wywołująca aktualizację innych zależnych od niej
    elementów
    """

    def collectData(self):
        if self.lastRow is None:
            return
        data = self.lastRow
        for key in self.paramsNames:
            self.valueBuffer[key].append(data[key])
        self.updateTimer.singleShot(self.timeIntervalInSeconds * 1000, self.collectData)

    """
    Metoda przechwyjąca wczytany z zewnątrz wiersz danych
    """

    def insertData(self, data):
        self.lastRow = data
        self.newDataInside.emit()

    """
    Metoda zwracająca ostatni otrzymany wiersz danych
    Zwraca:
        - słownik indeksowany po nazwach parametrów
    """

    def getLastDataRow(self):
        return self.lastRow

    """
    Metoda zmieniająca konfigurację ( plik wynikowy i okres próbkowania ).
    Argumenty:
        outputFilePath - ścieżka do pliku w którym mają zostać zapisane dane
        timeIntervalInSeconds - czas pomiędzy kolejnymi próbkami zapisywanymi do pliku
    """

    def changeConfiguration(self, outputFilePath, timeIntervalInSeconds):
        self.outputPath = outputFilePath
        self.timeIntervalInSeconds = timeIntervalInSeconds
        self.updateTimer.singleShot(self.timeIntervalInSeconds * 1000, self.collectData)
        self.savingTimer.singleShot(self.timeIntervalInSeconds * 1000 * 10, self.appendDataToFile)
