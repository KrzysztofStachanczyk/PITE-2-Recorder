from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
# for quick IP validation
import socket

"""
Dialog odpowiedzialny za pobranie danych do połącznia UDP i wstępną ich walidację
"""


class ConnectionDialog(QDialog):
    def __init__(self, base=None):
        super(ConnectionDialog, self).__init__(base)

        self.IP = "127.0.0.1"
        self.PORT = 50000

        self.myLayout = QVBoxLayout(self)
        self.myLayout.addWidget(QLabel("Data provider IP:"))
        self.IPField = QLineEdit(self)
        self.IPField.setText(self.IP)

        self.myLayout.addWidget(self.IPField)
        self.myLayout.addWidget(QLabel("Data provider port:"))

        self.portField = QSpinBox(self)
        self.portField.setRange(49152, 65535)
        self.portField.setValue(self.PORT)

        self.myLayout.addWidget(self.portField)
        self.applyButton = QPushButton(base)
        self.applyButton.setText("Connect")
        self.myLayout.addWidget(self.applyButton)

        self.applyButton.clicked.connect(self.checkData)

    """
    Wstępnie sprawdza dane zawarte w polach GUI pod kątem poprawności
    Jeżeli są one zgodne zamyka okno dialogowe
    """

    def checkData(self):
        validData = True

        try:
            socket.inet_aton(self.IPField.text())
        except socket.error:
            validData = False

        if validData:
            self.IP = self.IPField.text()
            self.PORT = self.portField.value()
            self.accept()

    """
    Zwraca krotkę zawierającą pobrany wcześniej z GUI adres IP oraz numer hosta
    """

    def getConnectionData(self):
        return self.IP, self.PORT

    """
    Metoda statyczna wywołująca okno dialogowe
    """

    @staticmethod
    def showDialog(base=None):
        dialog = ConnectionDialog(base)
        dialog.exec_()
        return dialog
