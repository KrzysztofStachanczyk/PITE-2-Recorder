# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_mainWidget.ui'
#
# Created: Sat Apr  2 00:25:02 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_mainWidget(object):
    def setupUi(self, mainWidget):
        mainWidget.setObjectName("mainWidget")
        mainWidget.resize(773, 432)
        self.verticalLayout = QtWidgets.QVBoxLayout(mainWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label = QtWidgets.QLabel(mainWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout_6.addWidget(self.label)
        self.dataproviderConnectButton = QtWidgets.QPushButton(mainWidget)
        self.dataproviderConnectButton.setObjectName("dataproviderConnectButton")
        self.horizontalLayout_6.addWidget(self.dataproviderConnectButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.label_2 = QtWidgets.QLabel(mainWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_6.addWidget(self.label_2)
        self.changeOutputConfigurationButton = QtWidgets.QPushButton(mainWidget)
        self.changeOutputConfigurationButton.setObjectName("changeOutputConfigurationButton")
        self.horizontalLayout_6.addWidget(self.changeOutputConfigurationButton)
        self.verticalLayout.addLayout(self.horizontalLayout_6)
        self.tabular = QtWidgets.QTabWidget(mainWidget)
        self.tabular.setObjectName("tabular")
        self.general = QtWidgets.QWidget()
        self.general.setObjectName("general")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.general)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.basicWidget = GeneralDataWidget(self.general)
        self.basicWidget.setObjectName("basicWidget")
        self.horizontalLayout.addWidget(self.basicWidget)
        self.tabular.addTab(self.general, "")
        self.verticalLayout.addWidget(self.tabular)

        self.retranslateUi(mainWidget)
        self.tabular.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(mainWidget)

    def retranslateUi(self, mainWidget):
        _translate = QtCore.QCoreApplication.translate
        mainWidget.setWindowTitle(_translate("mainWidget", "Widget"))
        self.label.setText(_translate("mainWidget", "Connect to data provider:"))
        self.dataproviderConnectButton.setText(_translate("mainWidget", "connect"))
        self.label_2.setText(_translate("mainWidget", "Output configuration:"))
        self.changeOutputConfigurationButton.setText(_translate("mainWidget", "change"))
        self.tabular.setTabText(self.tabular.indexOf(self.general), _translate("mainWidget", "Basic informations"))


from GeneralDataWidget import GeneralDataWidget
