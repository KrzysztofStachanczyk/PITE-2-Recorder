import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from ConnectionDialog import ConnectionDialog
from DataProvider import DataProvider
from DataCollector import DataCollector
from OutputConfigDialog import OutputConfigDialog
from ui_mainwidget import Ui_mainWidget

"""
Główna klasa odpowiedzialna za zarządzanie GUI oraz łączenie odpowiednich sygnałów i slotów
"""


class Application(QWidget):
    """
    Sygnał generowany w przypadku rządania połączenia z aplikacją dostarczającą
    dane dla recordera.
    Argumenty:
        str- adres IPv4
        int - numer portu na który będą przychodzić dane
    """
    requestConnectionTo = pyqtSignal(str, int)

    """
    Sygnał generowany w przypadku zmiany pliku wyjściowego dla danych
    Argumenty:
        str - ścieżka do nowego pliku
        int - interwał pomiędzy zapisami ( liczony w sekundach )
    """
    requestChangingOfOutputConfiguration = pyqtSignal(str, int)

    def __init__(self, base=None):
        QWidget.__init__(self, base)
        self.ui = Ui_mainWidget()
        self.ui.setupUi(self)
        self.dataProvider = DataProvider()
        self.dataCollector = DataCollector()

        self.ui.basicWidget.setDataCollector(self.dataCollector)

        # connect signals to slots
        self.ui.dataproviderConnectButton.clicked.connect(self.dataproviderConnectButton_clicked)
        self.ui.changeOutputConfigurationButton.clicked.connect(self.changeOutputConfigurationButton_clicked)
        self.requestConnectionTo.connect(self.dataProvider.connectTo)
        self.dataProvider.newData.connect(self.dataCollector.insertData)
        self.dataCollector.newDataInside.connect(self.ui.basicWidget.updateRequest)
        self.requestChangingOfOutputConfiguration.connect(self.dataCollector.changeConfiguration)

    """
    Slot wywoływany przy wciśnięciu przycisku odpowiedzialnego za nawiązanie nowego połączenia UDP.
    Wywołuje odpowiednie okno dialogowe i emituje sygnał rządania utworzenia połączenia
    """

    def dataproviderConnectButton_clicked(self):
        dialog = ConnectionDialog.showDialog()
        IP, port = dialog.getConnectionData()
        self.requestConnectionTo.emit(IP, port)

    """
    Slot wywoływany przy wciśnięciu przycisku zmiany konfiguracji pliku wynikowego.
    Wyświetla okno dialogowe i emituje sygnał requestChangingOfOutputConfiguration
    """

    def changeOutputConfigurationButton_clicked(self):
        dialog = OutputConfigDialog.showDialog()
        filePath, updateInterval = dialog.getData()
        self.requestChangingOfOutputConfiguration.emit(filePath, updateInterval)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = Application()
    w.show()
    sys.exit(app.exec_())
