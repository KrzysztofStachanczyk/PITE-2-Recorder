# Repozytorium projektu symulatora recordera do czarnej skrzynki #
**Uruchomienie FGFS z wypluwaniem danych:**

```
#!bash

 fgfs --generic=socket,out,20,localhost,1234,udp,playback 
```

**Dla projektu:**

```
#!bash
 fgfs --generic=socket,out,20,localhost,5000,udp,myProtocol 
```

Plik myProtocol.xml powinien znajdować się w katalogu $FG_ROOT/protocols/

**Druga część projektu dostępna pod adresem: **
https://github.com/KrzysztofStachanczyk/PITE-2-DataAnalizer

**Sprawozdanie z wykonania projektu w formie doc:**
https://docs.google.com/document/d/1XZtJud2hWtb5ZGz4HoKQqtu32BmdLCrmQwEw5Vw5fXg/edit?usp=sharing


# Sprawozdanie z projektu:

## Cel:
Stworzenie aplikacji rejestrującej dane lotu, zapisującej je na dysku. Oraz napisanie drugiego programu analizującego w.w dane. 

## Założenia wstępne:
Wykorzystanie symulatora lotu FlightGear jako źródła danych zbieranych w regularnych konfigurowalnych przez użytkownika odstępach czasu.
Uzyskanie stabilności pracy bez względu na przerwy i opóźnienia w dostarczaniu danych.
Budowa GUI umożliwiającego intuicyjne używanie aplikacji.
Zastosowanie sposoby zapisu pozwalającego na minimalizację rozmiaru plików.
Wyświetlanie na bieżąco rejestrowanych danych poprzez aplikację nagrywającą
Stworzenie programu wczytującego dane przy z stworzonego wcześniej pliku, wyrysowującego je na ekranie oraz uzyskującego z nich bardziej złożone informację.

## Wykorzystywane biblioteki i pakiety:
- NumPy -  wydajne przetwarzanie danych ( zamiana jednostek i inne operacje numeryczne) w aplikacji wyświetlającej dane w formie wykresów.
- PyQT5 - budowa graficznego interface-u użytkownika oraz wykorzystanie wbudowanych w bibliotekę nieblokujących gniazdek UDP
- pickle - zapis/odczyt uzyskanych danych w sposób binarny z zachowaniem ich struktury
- mathplotlib - rysowanie wykresów badanych parametrów
- matplotlib.backends.backend_qt5agg - możliwość osadzania wykresów z mathplotlib jako widgetów QT

## Wykorzystane narzędzia:
- Wireshark - diagnostyka przesyłanych po UDP danych
- QtCreator - tworzenie formularzy GUI
- pyuic5 - generowanie kodu pozwalającego na utworzenie rozkładu elementów GUI na podstawie plików .ui
- PyCharm - IDE do programowania w Pythonie
- FlightGear - źródło danych do analizy   
- Dia - diagramy zawarte w sprawozdaniu
- SmartGit - graficzna nakładka na git-a

## Format przechwytywanych danych:

Program FlightGear umożliwia różne sposoby przekazywania danych do zewnętrznego softu oparte między innymi na plikach i połączeniach TCP / UDP. W projekcie wykorzystano połączenie UDP ze względu na prostotę implementacji przy jednoczesnym zachowaniu zalet komunikacji sieciowej.  Aby zapewnić responsywność aplikacji zastosowano gniazdo typu non-blocking nasłuchujące na wskazanym przez użytkownika porcie. Uzyskano w ten sposób również gwarancję tego, że nadchodzące dane nie będą tracone w momencie gdy aplikacja wykonuje inne zadania (buforowanie danych). 

W warstwie aplikacji stosu TCP/IP zastosowano prosty protokół tekstowy umożliwiający  szybki wgląd w przekazywane dane jeszcze przed stworzeniem aplikacji i ich weryfikację (do tego celu zastosowano narzędzie WireShark).

Zgodnie z dokumentacją programu FlightGear opis w.w. protokołu został stworzony jako dokument XML zawierający informację o:
- znaku separacji kolejnych danych ( wybrano “:”) 
- nazwach sprawdzanych parametrów, ścieżkach prowadzących do węzłów je zawierających
- typ oraz format przekazywanych wartości - dla wszystkich parametrów wybrano zmienną typu float bez dodatkowego formatowania

Definicja protokołu znajduje się w pliku myProtocol.xml

Aby aplikacja FlightGear rozpoczęła nadawanie komunikatów według w.w. protokołu należy umieścić plik XML wewnątrz katalogu $FG_ROOT/protocols/  i uruchomić symulator z flagami:
```
fgfs --generic=socket,out,20,localhost,50000,udp,myProtocol 
```
Gdzie 50000 to numer portu na który są wysyłane dane.

## Zbierane dane:

FlightGear posiada tysiące nodów dostarczających różne dane dotyczące lotu do realizacji programu wybrano następujące:

Nazwa parametru | Ścieżka do węzła
--- | --- 
throttle-left | /controls/engines/engine[0]/throttle
throttle-right | /controls/engines/engine[1]/throttle
latitude-deg | /position/latitude-deg
longitude-deg | /position/longitude-deg
altitude-ft | /position/altitude-ft
altitude-to-ground-ft | /position/altitude-agl-ft
roll-deg | /orientation/roll-deg
pitch-deg | /orientation/pitch-deg
heading-deg | /orientation/heading-deg
airspeed-kt | /velocities/airspeed-kt
flap-pos-norm | /surface-positions/flap-pos-norm[0]
gear-down | /controls/gear/gear-down
brake-parking | /controls/gear/brake-parking

Wartości niezgodne z typem float takie jak gear-down są rzutowane w następujący sposób 0- false 1-true.

## Elementy programu zbierającego dane:
- DataProvider - zapewnia odbieranie danych, zrywanie i nawiązywanie połączenia sieciowego, emituje sygnały newData(dict) zwracające wiersz wczytanych danych w przypadku odebrania nowych danych ( po uprzednim ich przetworzeniu z łańcuchu znakowego na słownik ).
- DataCollector - zarządza danymi, przeprowadza ich buforowanie oraz zapis do pliku. Emituje sygnał newDataInside wywoływany za każdym razem gdy minął zadany przez użytkownika czas a aplikacja otrzymała nowe dane.
- ConnectionDialog - element GUI odpowiedzialny za pobranie od użytkownika danych do połączenia UDP i wstępną walidację ich poprawności.  
- GeneralDataWidget - widget odpowiedzialny za wyświetlanie ostatniego odebranego wiersza danych w formie graficznej
- OutputConfigDialog - element GUI odpowiedzialny za zebranie informacji o miejscu zapisu pliku z danymi i przerwie pomiędzy rekordami
- ui_mainwidget.py, ui_generalinfo.py - odpowiadają za inicjalizację GUI

## Elementy programu wyświetlającego dane:
- Plotter - widget rysujący wykresy, napisany w taki sposób aby był możliwie uniwersalny i wspierał rysowanie wielu serii danych
- DataLoader - odpowiada za wczytanie z pliku danych o locie i konwersji ich na słownik. Emituje dwa sygnały zależne od tego czy operacja wczytywania się powiodła:
  - dataLoaded(dict)
  - loadingError()
- Application - główny element aplikacji, odpowiada za zarządzanie GUI i realizowanie prostych operacji tj. pobranie ścieżki pliku z danymi od użytkownika, zmiana jednostek z imperialnych na metryczne
- RowInspector - wypisuje dane pochodzących z wybranej próbki. Posiada również opcję odtworzenia z zadaną prędkością zmian parametrów.
- ui_mainwidget.py - odpowiada za inicjalizacje GUI


## Struktura danych:
Wiersz danych jest przechowywany jako słownik indeksowany po nazwie parametru. Natomiast do pliku zapisywany jest słownik list w.w elementów przez co możliwe jest przekazanie odpowiednim funkcją rysującym tylko takiej części danych, która jest wymagana do poprawnego działania 

## Realizacja GUI:
Aby uzyskać w pełni skalowalne GUI, które może dopasować się do rozmiarów okna zastosowano bibliotekę QT z jej bindingiem PyQT. Biblioteka ze względu na swoją strukturę wymusza zastosowanie modelu MVC, którego pewne elementy widoczne są wewnątrz aplikacji. Celem zapewnienia zgodności z jej mechanizmami oraz jednorodności kodu elementy logiki biznesowej również komunikują się przy pomocy sygnałów i slotów. 

GUI był tworzony przy pomocy programu QTCreator. Następnie powstałe w ten sposób formularze *.ui zostały przetworzone narzędziem pyuic5 na pliki zawierające instrukcje języka Python3 (PyQT) pozwalające na stworzenie zaprojektowanych wcześniej układów elementów. 

## W projekcie wykorzystano następujące elementy podstawowe GUI:
- Push Button
- Label
- LCD Number
- Spin Box
- Horizontal and Vertical Spacer
- Tab Widget
- Progress Bar
- Vertical and Horizontal Layout

Do rysowania wykresów zastosowano FigureCanvas z biblioteki mathplotlib osadzony jako widget QT.

## Podsumowanie:
Większość założeń projektu została zrealizowana. W celu zwiększenia responsywności warto byłoby zastanowić się nad przeniesieniem operacji na plikach do osobnego wątku. W czasie testów program działał stabilnie i nie były widoczne opóźnienia związane z operacjami zapisu. 

Binarny format pliku pozwolił zredukować jego rozmiar do mniej niż 150 bajtów na próbkę co przy siedmiogodzinnym zapisie wykonywanym co sekundę daje rozmiar rzędu 3,6 MB, który jest wartością dopuszczalną.

Przydatną opcją byłoby również powiadamianie użytkownika o nieotrzymywaniu danych przez dłuższy czas. 
